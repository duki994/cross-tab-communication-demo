import { CrossTabService } from './common/cross-tab/cross-tab.service';
import { Component, OnInit } from '@angular/core';
import { merge, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ctdemo';
  messageReceived$!: Observable<string>;

  simpleInputControl = new FormControl('', [Validators.required]);

  dataCrossTabChannel = this.crossTab.getChannel('data');
  constructor(private crossTab: CrossTabService) { }


  ngOnInit(): void {
    this.messageReceived$ = merge(of('Other tab not open, or didn\'t receive message'),
      this.dataCrossTabChannel.message$
        .pipe(
          map(data => `${data?.zec}`)
        ));

      console.log('init');
  }

  send() {
    this.dataCrossTabChannel.send({ zec: this.simpleInputControl.value });
  }
}
