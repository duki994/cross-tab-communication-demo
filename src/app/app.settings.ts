/** Settings for the Cross Tab service which is responsible for sending messages between tabs */
export const CROSS_TAB_SETTINGS = {
  // Prefix for LocalStorage keys, which will be used for prefixing message keys.
  // Used when BroadcastChannel API is not available
  localStoragePrefix: '__Cross-Tab-MSG__',
};
