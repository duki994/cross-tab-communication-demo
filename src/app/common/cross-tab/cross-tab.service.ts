/*
  This service allows tabs with the same origin to communicate by broadcasting and receiving messages
  The service will use the BroadcastChannel API, if supported by the host environment.
  Otherwise (e.g. in Internet Explorer) it will try to use the LocalStorage API.

  STEP 1: ____________________________________________________________________________________________________
      To configure a typing for a new communication channel, edit interface CrossTabChannelTypes
        interface: CrossTabChannelTypes
        Location: ./cross-tab-channel.class.ts

      For example, to add a type for new Channel called 'login', which would send data of type YourMessageType,
        export interface CrossTabChannelTypes {
            login: boolean; // This is sample - remove
            logout: string; // This is sample - remove
        }


  STEP 2: ____________________________________________________________________________________________________
      In your component class you can use the CrossTabService as follows:

      import { CrossTabService } from 'src/app/shared-module/services/cross-tab/cross-tab.service';

      @Component({})
      export class MyComponent {

        loginChannel = this.corsstab.getChannel('login');
        loginSubscription = this.loginChannel.message$.subscribe(msg => processMessage(msg));

        // Broadcase to all other tabs with the same origin and this service
        send(msg: YourMessageType) {
          this.loginChannel.send(msg);
        }

        constructor(
          private crossTabService: CrossTabService, // Import the CrossTabService
        ) { }

 */


import { Injectable, NgZone } from '@angular/core';
import { fromEvent } from 'rxjs';
import { share } from 'rxjs/operators';
import { Counter } from './classes/counter.class';
import { ChannelStorage } from './classes/channel-storage.class';
import { ChannelBroadcast } from './classes/channel-broadcast.class';
import { CrossTabChannel, CrossTabChannelTypes } from './cross-tab.service.model';
import { CROSS_TAB_SETTINGS as SETTINGS } from 'src/app/app.settings';


@Injectable({
  providedIn: 'root'
})
export class CrossTabService {

  private storage$ = fromEvent<StorageEvent>(window, 'storage').pipe(share());
  private counter = new Counter();
  private broadcastAPI = !!window.BroadcastChannel; // Whether the host environment (browser) support BroadcastChannel API

  readonly instanceId = new Date().getTime() + Math.floor(Math.random() * 1e16);

  getChannel<ChannelType extends keyof CrossTabChannelTypes>(channelType: ChannelType): CrossTabChannel<typeof channelType> {
    return this.broadcastAPI
      ? new ChannelBroadcast(channelType, this.ngZone)
      : new ChannelStorage(channelType, SETTINGS.localStoragePrefix, this.storage$, this.instanceId, this.counter);
  }

  constructor(
    private ngZone: NgZone
  ) { }

}
