import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CrossTabChannel, CrossTabChannelTypes } from '../cross-tab.service.model';
import { Counter } from './counter.class';

const zec = '';

interface StoredItemWrapper<ChannelType extends keyof CrossTabChannelTypes> {
  type: string; // Type of data stored. `undefined`, `string`, `number`, `object`, etc.
  data: CrossTabChannelTypes[ChannelType]; // Actual message being stored
  instance: number; // Identifies the tab originating the message. Used to prevent receiveing own messages - issue for IE
  index: number; // A unique number for each message to make sure that even a repeated message is still sent
  global: boolean;
}



export class ChannelStorage<ChannelType extends keyof CrossTabChannelTypes> implements CrossTabChannel<ChannelType> {

  private outbox$ = new Subject<Omit<StoredItemWrapper<ChannelType>, 'from'>>();

  /** This observable emits messages sent broadcast by tabs with same origin and using this same service */
  message$: Observable<CrossTabChannelTypes[ChannelType]>;

  /** Sends messages to other tabs with the same origin, which have this Angular service */
  send(message: CrossTabChannelTypes[ChannelType], global = false): void {
    this.outbox$.next({
      type: typeof message, // Type of data stored. `undefined`, `string`, `number`, `object`, etc.
      data: message, // Actual message being stored
      instance: this.instanceId, // Identifies the tab originating the message. Used to prevent receiveing own messages - issue for IE
      index: this.counter.next, // A unique number for each message to make sure that even a repeated message is still sent
      global
    });
  }

  constructor(
    private channelType: ChannelType,
    private prefix: string,
    storage$: Observable<StorageEvent>,
    private instanceId: number, // Identifies current instance. Otherwise tabs in Internet Explorer will receive own messages
    private counter: Counter,
  ) {
    this.message$ = storage$.pipe(
      filter(e => e.key === this.channelKey), // Filter out all messages not meant for the current channel
      map(e => this.fromString(e.newValue ?? '')), // Extract the message, still wrapped together with meta data
      filter(wrapped => wrapped?.instance !== this.instanceId), // Filter messages from self - is an issue for IE (StorageEvent is fired for current tab too)
      map(wrapped => wrapped.data) // Extract actual messaage from the wrapper
    );

    this.outbox$.subscribe(message => {
      localStorage.setItem(this.channelKey, this.toString({ ...message }));
    });
  }

  private get channelKey(): string {
    return this.prefix + this.channelType;
  }

  private toString(message: StoredItemWrapper<ChannelType>): string {
    return JSON.stringify(message);
  }

  private fromString(s: string): StoredItemWrapper<ChannelType> {
    return JSON.parse(s);
  }
}
