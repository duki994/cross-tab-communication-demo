import { NgZone } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { CrossTabChannel, CrossTabChannelTypes, MetaMessage } from '../cross-tab.service.model';

export class ChannelBroadcast<T extends keyof CrossTabChannelTypes> implements CrossTabChannel<T> {

  private broadcastChannel: BroadcastChannel;
  private outbox$ = new Subject<MetaMessage<T>>();

  private message = new Subject<MetaMessage<T>>();
  message$: Observable<CrossTabChannelTypes[T]>;

  send(data: CrossTabChannelTypes[T], global = false): void {
    this.outbox$.next({ data, global });
  }

  constructor(channelName: T, private zone: NgZone) {
    this.broadcastChannel = new BroadcastChannel(channelName);
    this.broadcastChannel.onmessage = message => this.zone.run(() => this.message.next(message.data));
    this.broadcastChannel.onmessageerror = message => this.zone.run(() => this.message.error(message.data));

    this.message$ = this.message.pipe(
      map((metaMessage) => metaMessage.data)
    );

    this.outbox$.subscribe(message => this.broadcastChannel.postMessage({
      global: message.global,
      data: message.data
    } as MetaMessage<T>));
  }

}
