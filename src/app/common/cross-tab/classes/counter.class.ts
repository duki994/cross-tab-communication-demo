export class Counter {

  constructor(private index = 0) { }

  get next() {
    return ++this.index;
  }
}
