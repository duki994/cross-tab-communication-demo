import { Observable } from 'rxjs';

export interface IEditMode {
  [k: string]: any;
  [k: number]: any;
  editMode: boolean;
  fromOtherTab?: boolean;
}


export interface CrossTabChannel<T extends CrossTabChannelKey> {
  message$: Observable<CrossTabChannelTypes[T]>;
  send(data: CrossTabChannelTypes[T], global?: boolean): void;
}

/** Each Key and Value pair of the following interface describes a communication 'Channel'
 *
 *  Let us consider the following example:
 *
 *      interface CrossTabChannelTypes {
 *          login: { success: boolean; }
 *          age: number;
 *      }
 *
 *  This would describe two available channels.
 *      1. The 'login' channel which can send /receive messages { success: true } and { success: false }
 *      2. The 'age' channel, which can send / receive numbers
 */
export interface CrossTabChannelTypes {
  data: { [k: string]: any }; // for demo purposes only
}

export type CrossTabChannelKey = keyof CrossTabChannelTypes;

export interface MetaMessage<T extends CrossTabChannelKey> {
  global: boolean;
  data: CrossTabChannelTypes[T];
}
