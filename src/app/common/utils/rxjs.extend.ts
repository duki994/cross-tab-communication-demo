import { MonoTypeOperatorFunction, Observable, OperatorFunction, pipe } from 'rxjs';
import { take } from 'rxjs/operators';


/**
 * Combines each value from source1 observable with the first value of source2 observable.
 * Will only start emiting once source2 emits its first value.
 * At this point all previous values from source1 will also be combined the first value from source2 and emitted
 * Example:
 *          source1.pipe(
 *              withEarliestFrom(source2)
 *          ).subscribe(([val1, val2]) => { ... })
 */
export function withEarliestFrom<T1, T2>(source2: Observable<T2>): OperatorFunction<T1, [T1, T2]> {
    return source1 => new Observable(o => {
        let s1Cache: T1[] = [];
        let s2Cache: T2;
        let s2Ready = false;

        const s1 = source1.subscribe(val1 => {
            if ( s2Ready ) {
                o.next([val1, s2Cache]);
            } else {
                s1Cache.push(val1);
            }
        });

        const s2 = source2.pipe(
            take(1)
        ).subscribe(val2 => {
            s2Cache = val2;
            s2Ready = true;
            s1Cache.forEach(v1 => o.next([v1, val2]));
            s1Cache = null;
        });

        return function taredown() {
            s1.unsubscribe();
            s2.unsubscribe();
        };
    });
}

/**
 * Takes observable and returns similar observable which emits same values and error and complete events, but
 * will call the predicate on the last value
 * @param predicate function to call on the last emitted value
 * @param onError true if predicate should be called even if error is thrown (with parameter last value before error)
 */
export function tapLast<T>(predicate: (t: T) => any, onError = true): MonoTypeOperatorFunction<T> {
    return source => new Observable(subscriber => {
        let seen = false;
        let last: T;
        function safePredicate(isError: boolean) {
            if ( seen && (! isError || onError) ) { predicate(last); }
        }
        const subscription = source.subscribe({
            next: value => (last = value, seen = true, subscriber.next(value)),
            error: err => (safePredicate(true), subscriber.error(err)),
            complete: () => (safePredicate(false), subscriber.complete())
        });
        return function taredown() {
            subscription.unsubscribe();
        };
    });
}

export const noop = <T>() => pipe<Observable<T>>();
