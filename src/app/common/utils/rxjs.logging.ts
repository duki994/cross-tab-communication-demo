import { MonoTypeOperatorFunction, Observer, PartialObserver, pipe } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LogLevel, minimumLogLevel } from './logging/log-level';
import { noop } from './rxjs.extend';

/**
 *
 * Useful for logging. Wraps `tap` operator and executes it if current log level is satisfied.
 * Otherwise just returns source observable without passing it through `tap` operator
 * @param logLevel Current log level
 * @param next Observer `next` callback
 * @param error Observer `error` callback
 * @param complete Observer `complete` callback
 */
export function log<T>(logLevel: LogLevel,
                       next?: (x: T) => void, error?: (e: any) => void, complete?: () => void): MonoTypeOperatorFunction<T>;
/**
 * * Useful for logging. Wraps `tap` operator and executes it if current log level is satisfied.
 * Otherwise just returns source observable without passing it through `tap` operator
 * @param logLevel Current log level
 * @param observer Partial observer used for logging purposes.
 */
export function log<T>(logLevel: LogLevel,
                       observer: PartialObserver<T>): MonoTypeOperatorFunction<T>;
export function log<T>(logLevel: LogLevel,
                       observerOrNext?: Partial<Observer<T>> | ((value: T) => void) | null,
                       error?: ((e: any) => void) | null,
                       complete?: (() => void) | null): MonoTypeOperatorFunction<T> {

  const skipLog = logLevel < minimumLogLevel || logLevel === LogLevel.None;
  if (skipLog) {
    return noop<T>();
  }

  const observer =
    typeof observerOrNext === 'function' || error || complete // if either is function
      ? { next: observerOrNext as (value: T) => void, error, complete }
      : observerOrNext;

  return pipe(tap(observer as PartialObserver<T>));

}
